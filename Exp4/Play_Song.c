#include "stdio.h"
#include "main.h"
#include "PORT.H"
#include "SPI_Driver.h"
#include "File_System_struct.h"
#include "Read_Sector.h"
#include "Outputs.h"
#include "Play_Song.h"

extern uint8_t xdata buf1[512];
extern uint8_t xdata buf2[512]; 

void Play_Song(uint32_t Start_Cluster)
{
   uint16_t index1, index2;
   uint8_t buffer1, buffer2, temp8;
   uint32_t sector, sector_offset;
   printf("\r\nStarting Cluster = %lu\n\r",Start_Cluster);

   sector=First_Sector(Start_Cluster);
   printf("Starting Sector = %lu\n\r",sector);
   sector_offset=0;
   buffer1=1;
   buffer2=0;
   //Clear_P2_bit(Red_LED);  P3_2=ON;
   LED_On(Yellow_LED);   //YELLOWLED=ON;
   index1=0;
   
   Read_Sector(sector+sector_offset, 512, buf1);

   sector_offset++;
   LED_Off(Yellow_LED);  //YELLOWLED=OFF;
   LED_On(Amber_LED);   //AMBERLED=ON;
   index2=0;
   
   Read_Sector(sector+sector_offset, 512, buf2);
   sector_offset++;
   LED_Off(Amber_LED);  //AMBERLED=OFF;
   do
  {      
     do
     {
        if(DATA_REQ==0)
        {
           LED_On(Green_LED);   //GREENLED=ON;
           Port_Set_bits(PORT_1, BIT_EN_bit);  //BIT_EN=1;
           SPI_Transfer(buf1[index1], &temp8);
           Port_Clear_bits(PORT_1, BIT_EN_bit);  //BIT_EN=0;
	       LED_Off(Green_LED);  //GREENLED=OFF;
	       index1++;
           if(index1>511)
           {
              if(index2>511)
              {
                  //BIT_EN=0;              
                  LED_On(Amber_LED);   //AMBERLED=ON;
				  index2=0;
				  
				  Read_Sector(sector+sector_offset, 512, buf2);
				  sector_offset++;
                  LED_Off(Amber_LED);  //AMBERLED=OFF;
              }
              buffer1=0;
              buffer2=1;

          }
       }
       else
       {
          if(index2>511)
          {
              //BIT_EN=0;
              LED_On(Amber_LED);   //AMBERLED=ON;
			  index2=0;
			  
			  Read_Sector(sector+sector_offset, 512, buf2);
			  sector_offset++;
              LED_Off(Amber_LED);  //AMBERLED=OFF;
          }
          else
          {
              if(index1>511)
              {
                  buffer1=0;
                  buffer2=1;
              }
          }
      }
   }while(buffer1==1);
   do
   {
      if(DATA_REQ==0)
      {
          LED_On(Red_LED);   //REDLED=ON;
          Port_Set_bits(PORT_1, BIT_EN_bit);  //BIT_EN=1;
          SPI_Transfer(buf2[index2], &temp8);
          Port_Clear_bits(PORT_1, BIT_EN_bit);  //BIT_EN=0;
          LED_Off(Red_LED);  //REDLED=OFF;
          index2++;
          if(index2>511)
          {
              if(index1>511)
              {
                  //BIT_EN=0; 
                  LED_On(Yellow_LED);   //YELLOWLED=ON;
				  index1=0;
				  
				  Read_Sector(sector+sector_offset, 512, buf1);
				  sector_offset++;
                  LED_Off(Yellow_LED);  //YELLOWLED=OFF;
              }
              buffer2=0;
              buffer1=1;
         
           }
        }
        else
        {
           if(index1>511)
           {
              //BIT_EN=0; 
              LED_On(Yellow_LED);   //YELLOWLED=ON;
			  index1=0;
			  
			  Read_Sector(sector+sector_offset, 512, buf1);
			  sector_offset++;
              LED_Off(Yellow_LED);  //YELLOWLED=OFF;
           }
           else
           {
               if(index2>511)
               {
                  buffer2=0;
                  buffer1=1;
               }
           }
        }
      }while(buffer2==1);
  }while(sector_offset<512); // Plays 8 clusters assumming that they are continguous
//P3_2=OFF;
} 




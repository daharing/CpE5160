/*-------------------------------------
|           PRIVATE INCLUDES           |
--------------------------------------*/
#include "STA013.h"

#include "Main.h"
#include "PORT.H"
#include "Outputs.h"

#include "I2C.h"
#include <stdio.h> /* for printf */

/*-------------------------------------
|        EXTERNAL DECLARATIONS         |
--------------------------------------*/
extern uint8_t code CONFIG;
extern uint8_t code CONFIG2;

/*-------------------------------------
|           PRIVATE STATICS            |
--------------------------------------*/
static int _device_id = 0;

/*-------------------------------------
|         FUNCTION DEFINITIONS         |
--------------------------------------*/

uint8_t STA013_Init(void)
{
   /*-------------------------------------
   |              VARIABLES               |
   --------------------------------------*/
   uint16_t i;
   uint8_t return_val;
   uint8_t *config_data;
   uint8_t timeout;
   uint8_t reg_addr;
   uint8_t recv_arry[3];
   uint8_t idata send_arry[3];

   uint8_t code sta_cfg_addr[STA_CFG_SZ] = {0x54, 0x55, 6, 11, 82, 81, 101, 100, 80, 97, 0x05, 0x0d, 0x18, 0x0c, 0x46, 0x48, 0x7d, 0x72};
   uint8_t code sta_cfg_data[STA_CFG_SZ] = {0x07, 0x10, 9, 2, 184, 0, 0, 0, 6, 5, 0xa1, 0x00, 0x04, 0x05, 0x07, 0x07, 0x07, 0x01};


   /*-------------------------------------
   |         LOOP INITIALIZATIONS         |
   --------------------------------------*/
   Port_Clear_bits(PORT_3, STA013_RESET_PIN);
   send_arry[0]      = 0x01;
   timeout           = 50;
   i = 0;

   /*-------------------------------------
   |            SET RESET PIN             |
   --------------------------------------*/
   Port_Set_bits(PORT_3, STA013_RESET_PIN);

   do
   {
      Port_Clear_bits(PORT_3, STA013_SIGNAL_PIN);// Used for debugging
      return_val = I2C_Read(STA013_ADDR, 1, 1, 1, recv_arry);
      Port_Set_bits(PORT_3, STA013_SIGNAL_PIN);// Used for debugging

      timeout--;
   } while ( timeout != 0 && return_val != ERR_NONE );

   if ( timeout != 0 )
   {
      printf("STA013 ID: 0x%2.2bX \n\r", recv_arry[0]);
      _device_id = (int)recv_arry[0];
   }
   else if( timeout == 0 )
   {
      printf("STA013 Init FAILED...ERR_TIMEOUT\n\r");
      return ERR_TIMEOUT;
   }

   if ( recv_arry[0] != 0xAC )
   {
      printf("WARNING: STA013 Init: ID is not 0xAC. Skip loading CONFIG files. \nReceived ID value is %u. I2C_Read return value is %u and should be %u.\n\n\r", recv_arry[0], return_val, ERR_NONE);
      return return_val;
   }

   /*-------------------------------------
   |         GET SOFTWARE VERSION         |
   --------------------------------------*/
   return_val = I2C_Read(STA013_ADDR, 0x71, 1, 1, recv_arry);
   // printf("STA013 Software Version: %2.2bX \n\r", recv_arry[0]);

   /*-------------------------------------
   |         LOOP INITIALIZATIONS         |
   --------------------------------------*/
   i = 0;
   config_data = &CONFIG;

   /*-------------------------------------
   |         SEND 1ST CONFIG FILE         |
   --------------------------------------*/
   do
   {
      reg_addr = config_data[i];

      i++;
      send_arry[0] = config_data[i];

      i++;
      if ( reg_addr == 0xFF )
      {
         continue;
      }

      /*-------------------------------------
      |        SUBLOOP INITIALIZATIONS       |
      --------------------------------------*/
      timeout = 50;

      do
      {
         timeout--;

         return_val = I2C_Write(STA013_ADDR, reg_addr, 1, 1, send_arry);
      } while ( return_val != ERR_NONE && timeout != 0 );
   } while ( reg_addr != 0xFF && timeout != 0 );

   if ( timeout == 0 )
   {
      printf("STA013 Init: FAILED: timed out sending first config file.\n\r");
      LED_On(Yellow_LED);
      return ERR_TIMEOUT;
   }

   printf("First configuration file sent...\n\r");

   /*-------------------------------------
   |         LOOP INITIALIZATIONS         |
   --------------------------------------*/
   i = 0;
   config_data = &CONFIG2;

   /*-------------------------------------
   |         SEND 2ND CONFIG FILE         |
   --------------------------------------*/
   do
   {
      reg_addr = config_data[i];

      i++;
      send_arry[0] = config_data[i];

      i++;
      if ( reg_addr == 0xFF )
      {
         continue;
      }

      /*-------------------------------------
      |       SUBLOOP INITIALIZATIONS        |
      --------------------------------------*/
      timeout = 50;

      do
      {
         timeout--;

         return_val = I2C_Write(STA013_ADDR, reg_addr, 1, 1, send_arry);
      } while ( return_val != ERR_NONE && timeout != 0 );
   } while ( reg_addr != 0xFF && timeout != 0 );

   if ( timeout == 0 )
   {
      printf("STA013 Init: FAILED: timed out sending second config file.\n\r");
      LED_On(Yellow_LED);
      return ERR_TIMEOUT;
   }

   /*-------------------------------------
   |         GET SOFTWARE VERSION         |
   --------------------------------------*/
   return_val = I2C_Read(STA013_ADDR, 0x71, 1, 1, recv_arry);
   printf("STA013 Software Version: %2.2bX \n\r", recv_arry[0]);


   for (i = 0; i < STA_CFG_SZ; i++)
   {
      reg_addr = sta_cfg_addr[i];
      send_arry[0] = sta_cfg_data[i];

      timeout = 50;
      do
      {
         return_val = I2C_Write(STA013_ADDR, reg_addr, 1, 1, send_arry);
         timeout--;
      } while ( return_val != ERR_NONE && timeout != 0 );

      if ( timeout == 0 )
      {
         LED_On(Yellow_LED);
         return ERR_TIMEOUT;
      }

      // printf("Sent to STA013: %2.2bX  %2.2bX \n\r", reg_addr, send_arry[0]);
      reg_addr = sta_cfg_addr[i];
      return_val = I2C_Read(STA013_ADDR, reg_addr, 1, 1, recv_arry);
      // printf("Output from register: %2.2bX  %2.2bX %2.2bX \n\r", reg_addr, recv_arry[0], return_val);
   }

   printf("STA013 Initialization Complete\n\r");

   return return_val;
} /* STA013_Init() */

int STA013_Get_Device_ID(void)
{
   return _device_id;
}

#ifndef STA013_H
#define STA013_H

/*-------------------------------------
|           PUBLIC INCLUDES            |
--------------------------------------*/
#include "Main.H"

/*-------------------------------------
|            PUBLIC DEFINES            |
--------------------------------------*/
#define STA013_ADDR         (0x43)
#define STA_CFG_SZ          (18)

#define STA013_RESET_PIN    (1 << 3)
#define STA013_SIGNAL_PIN   (1 << 4) /* P3.5 */

/*-------------------------------------
|     PUBLIC FUNCTION DECLARATIONS     |
--------------------------------------*/

/**
 * Initializes the STA013 by performing a hardware reset,
 * checking the ID register value, and sending the assembly
 * configuration data
 * 
 * @Returns Error flag
 */
uint8_t STA013_Init(void);

/**
 * @Returns Device ID after initialization
 */
int STA013_Get_Device_ID(void);

#endif /* STA013_H */

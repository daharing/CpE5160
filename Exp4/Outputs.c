/*-------------------------------------
|           PRIVATE INCLUDES           |
--------------------------------------*/

#include "Main.h"
#include "Outputs.h"


/*********************************************************************
*    LED Output Functions
********************************************************************/

void LED_On(uint8_t LED_bitmask)
{
  Port_Clear_bits( PORT_2, LED_bitmask );
} /* LED_On() */

void LED_Off(uint8_t LED_bitmask)
{
  Port_Set_bits( PORT_2, LED_bitmask);
} /* LED_Off() */

void Port_Set_bits(enum ENUM_PORTS port, uint8_t bitMask)
{
  switch (port)
  {
  case PORT_0:
    PORT0 |= bitMask;
    break;
  case PORT_1:
    PORT1 |= bitMask;
    break;
  case PORT_2:
    PORT2 |= bitMask;
    break;
  case PORT_3:
    PORT3 |= bitMask;
    break;
  
  default:
    break;
  }
} /* Port_Set_bits() */

void Port_Clear_bits(enum ENUM_PORTS port, uint8_t bitMask)
{
  switch (port)
  {
  case PORT_0:
    PORT0 &= (~bitMask);
    break;
  case PORT_1:
    PORT1 &= (~bitMask);
    break;
  case PORT_2:
    PORT2 &= (~bitMask);
    break;
  case PORT_3:
    PORT3 &= (~bitMask);
    break;
  
  default:
    break;
  }
} /* Port_Clear_bits() */

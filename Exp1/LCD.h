/*
* Author:       Dustin Haring
* Copyright:    Copyright (C) 2019 Dustin Haring - All Rights Reserved.
* Date Created: Sept. 24, 2019
* Desc:
*/

#ifndef LCD_H
#define LCD_H

/*-----------
|  INCLUDES  |
-----------*/

#include "Main.h"
#include "PORT.H"

/*----------
|  DEFINES  |
----------*/



/*------------------------
|  FUNCTION DECLARATIONS  |
------------------------*/

/* Write Command to LCD.
    RS = 0 for sending commands,
    else RS = 1 for sending LCD data */
void LCD_Write(uint8_t RS, uint8_t buff);

/* Write command byte to LCD */
void LCD_Write_Command(uint8_t command);

/* Write string data to LCD screen.
    -Returns number of bytes sent to LCD
        or negative number for error*/
int8_t LCD_Print_String(uint8_t *str, uint8_t sz);

/* Clear LCD Display and Reset Display Position to 0 */
void LCD_Clear(void);

/* Initialize the LCD screen */
void LCD_init();

#endif /* LCD_H */
/*
* Author:       Dustin Haring
* Copyright:    Copyright (C) 2019 Dustin Haring - All Rights Reserved.
* Date Created: Sept. 24, 2019
* Desc: 
*/

#ifndef UART_H
#define UART_H

#include "Main.h"

/* Initializes the UART given a baud rate */
void UART_init(uint32_t Baud_Rate);

/* Sends one byte 'data' over UART
    -Returns 'data' if successful, else error code */
uint8_t UART_TX(uint8_t data_byte);

/* Returns one byte received by UART
    else 0 if no data has been received */
uint8_t UART_RX( void );




#endif /* UART_H */
/*
* Author:       Dustin Haring
* Copyright:    Copyright (C) 2019 Dustin Haring - All Rights Reserved.
* Date Created: Sept. 24, 2019
* Desc: 
*/

#ifndef MAIN_H
#define MAIN_H

/*-----------------------
|  DEFINE CONFIGURATION  |
-----------------------*/

#define LCD_REVERSE_P0      (1)     /* if your Port 0 is wired in reverse order to the LCD, set this to '1'. Else use '0' or delete for normal wiring */

/*------------------------------------------------------------------
* NOTE WILL NEED TO EDIT THIS SECTION FOR EVERY PROJECT
* ------------------------------------------------------------------*/

// Must include the appropriate microcontroller header file here
#include "AT89C51RC2.h"

/* Oscillator Frequency (Hz) */
#define OSC_FREQ (18432000UL)


/*  Number of oscillations per instruction (12, etc)
*   12 - Original 8051 / 8052 and numerous modern versions
*   6 - Various Infineon and Philips devices, etc.
*   4 - Dallas 320, 520 etc.
*   1 - Dallas 420, etc.
*/
#define OSC_PER_INST (6)

//------------------------------------------------------------------
// SHOULD NOT NEED TO EDIT THE SECTIONS BELOW
//------------------------------------------------------------------

// Typedefs (see Chap 5) These types match with stdint.h  
typedef unsigned char uint8_t;
typedef unsigned int  uint16_t;
typedef unsigned long uint32_t;
typedef char int8_t;
typedef int  int16_t;
typedef long int32_t;

// Interrupts (see Chap 7)  
#define Timer_0_Overflow 1
#define Timer_1_Overflow 3
#define Timer_2_Overflow 5

#endif /* MAIN_H */

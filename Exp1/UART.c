/*
* Author:       Dustin Haring
* Copyright:    Copyright (C) 2019 Dustin Haring - All Rights Reserved.
* Date Created: Sept. 24, 2019
* Desc: 
*/

#include "UART.h"

#define SPD     (1)     /* Set to select fast baud rate generator, clear for slow generator. This and SMOD1 should probably match.  */
#define SMOD0   (0)     /* Set to select FE bit in SCON, else to select SM0 bit in SCON */
#define SMOD1   (1)     /* Set to double baud rate */

void UART_init(uint32_t Baud_Rate)
{
    /*--------------------------
    |  CONFIGURE PCON REGISTER  |
    --------------------------*/
    /*
    |SMOD1|SMOD0|-|POF|GF1|GF0|PD |IDL|
    |  1  |  0  |0| 0 | 0 | 0 |	0 | 0 | */
    
    /* Clear SMOD1 and SMOD0 */
    PCON &= 0x3F; /* 0b00111111 */

    /* Set SMOD0 and SMOD1 bits accordingly */
    PCON |= (SMOD0 << 6);
    PCON |= (SMOD1 << 7);


    /*--------------------------
    |  CONFIGURE SCON REGISTER  |
    --------------------------*/

    /* Set UART mode 1 mode 1 (8-bit UART) (SM0 and SM1)
        Clear SM2 for RS232
        Enable UART RX (REN)

    |SMO|SM1|SM2|REN|TB8|RB8|TI |RI |
    | 0 | 1 | 0 | 1 | 0 | 0 | 0 | 0 | */
    
    SCON = 0x50; // 0b01010000;


    /*--------------------------------
    |  CONFIGURE BAUD RATE GENERATOR  |
    --------------------------------*/
    /*
    |-|-|-|BRR|TBCK|RBCK|SPD|SRC|
    |0|0|0| 1 |  1 | 1  | 1 | 0 | */

    /* Disable Baud Rate Generator (BRR) and Clear BRR, TBCK, RBCK, SPD, and SRC */
    BDRCON &= 0xE0; // 0b11100000

    /* Set Baud Rate Generator value */
    BRL = (uint8_t) (256-((1+SMOD1)*(1+(5*SPD))*OSC_FREQ)/((uint32_t)32*OSC_PER_INST*Baud_Rate));

    /* Enable and Configure Baud Rate Generator */
    #if ( SPD == 1 )
        BDRCON |= SPD << 1; //0b00000010; Set SPD appropriately
    #endif
    BDRCON |= 0x1C; //0b00011100; Set BRR, TBCK, and RBCK


    /*------------------------------------------------
    |  INDICATE SYSTEM IS READY TO SEND/RECEIVE DATA  |
    ------------------------------------------------*/
    TI = 1;
    RI = 0;

} /* UART_init() */


uint8_t UART_TX( uint8_t data_byte )
{
    /* Ensure we aren't transmitting */
    while(TI == 0);

    /* Set buffer to data to TX */
    SBUF = data_byte;

    /* Indicate that SBUF is ready to be TX  */
    TI = 0;

    return data_byte;
} /* UART_TX() */


uint8_t UART_RX( void )
{
    /*------------
    |  VARIABLES  |
    ------------*/
    uint8_t temp;

    /* Wait until data has been RX into buffer */
    while(TI == 0 || RI == 0);

    /* Save buffer data */
    temp = SBUF;

    /* Indicate we are ready to RX another byte */
    RI = 0;

    return temp;
} /* UART_RX() */

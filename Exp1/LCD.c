/*
* Author:       Dustin Haring
* Copyright:    Copyright (C) 2019 Dustin Haring - All Rights Reserved.
* Date Created: Sept. 24, 2019
* Desc:
*/

/*-------------------
|  PRIVATE INCLUDES  |
-------------------*/

#include "LCD.h"
#include "helper_functions.h"

/*------------------
|  PRIVATE DEFINES  |
------------------*/

#define LCD_CMD_SET_DISP_POS (0x80)


/*-----------------------
|  FUNCTION DEFINITIONS  |
-----------------------*/

/* Write Command to LCD.
    RS = 0 for sending commands,
    else RS = 1 for sending LCD data */
void LCD_Write(uint8_t RS, uint8_t buff)
{
    if ( RS == 0)
    {
        LCD_RS = 0;
    }
    else
    {
        LCD_RS = 1;
    }
    
    LCD_EN = 1;
#if ( LCD_REVERSE_P0 )
    LCD_PORT_REVERSED = reverseBits_8( buff ); /* Due to my wiring the LCD port backwards, I had to create a reverse bits function */
#else
    LCD_PORT = buff; /* Due to my wiring the LCD port backwards, I had to create a reverse bits function */
#endif
    LCD_EN = 0;
    LCD_PORT_REVERSED = 0xFF;
    LCD_RS = 1;
} /* LCD_Write() */

/* Sends given command byte to LCD */
void LCD_Write_Command(uint8_t command)
{
    LCD_Write(0, command);
} /* LCD_Write_Command() */

// FIXME 
/* Write string data to LCD screen.
    -Returns number of bytes sent to LCD
        or negative number for error*/
int8_t LCD_Print_String(uint8_t *str, uint8_t sz)
{
    /*------------
    |  VARIABLES  |
    ------------*/

    uint8_t i;

    /*----------------
    |  VERIFICATIONS  |
    ----------------*/

    if ( sz > 40 )
    {
        return -1;
    }


    /*---------------------------------------------------
    |  RESET CURSOR POSITION/DISPLAY CHAR POSITION TO 0  |
    ---------------------------------------------------*/

    LCD_Write_Command( LCD_CMD_SET_DISP_POS | 0x00 );
    Delay_1ms(1);


    /*--------------
    |  SEND STRING  |
    --------------*/

    for(i = 0; i < sz; i++)
    {
        LCD_Write(1, str[i]);
        Delay_1ms(1);
    }

    return i;

} /* LCD_Print_String() */

void LCD_init()
{
    /*------------
    |  VARIABLES  |
    ------------*/
    uint8_t data_byte;

    /*----------------------
    |  DELAY ~100ms SECOND  |
    -----------------------*/

    Delay_1ms(100);


    /*------------------------
    |  LCD FUNCTION SET (x4)  |
    ------------------------*/

    // data_byte = 0x3C;       // 0b00111100
    data_byte = 0x38;       // 0b00111000
    LCD_Write_Command(data_byte);
    Delay_1ms(5);

    // data_byte = 0x3C;       // 0b00111100
    data_byte = 0x38;       // 0b00111000
    LCD_Write_Command(data_byte);
    Delay_1ms(1);
    
    // data_byte = 0x3C;       // 0b00111100
    data_byte = 0x38;       // 0b00111000
    LCD_Write_Command(data_byte);
    Delay_1ms(1);
    
    // data_byte = 0x3C;       // 0b00111100
    data_byte = 0x38;       // 0b00111000
    LCD_Write_Command(data_byte);
    Delay_1ms(1);


    /*-------------------------
    |  DISPLAY ON/OFF CONTROL  |
    -------------------------*/

    // data_byte = 0x0C;			// 0b00001100
    data_byte = 0x48;			// 0b01001000
    LCD_Write_Command(data_byte);
    Delay_1ms(10);


    /*----------------
    |  DISPLAY CLEAR  |
    ----------------*/

    data_byte = 0x01;			// 0b00000001
    LCD_Write_Command(data_byte);
    Delay_1ms(10);


    /*-----------------
    |  ENTRY MODE SET  |
    -----------------*/

    // data_byte = 0x07;           // 0b0000111
    data_byte = 0x0C;           // 0b00001100
    LCD_Write_Command(data_byte);
    Delay_1ms(10);


    /*-----------------------
    |  CURSOR/DISPLAY SHIFT  |
    -----------------------*/
    // data_byte = 0x1C;           // 0b00011100
    // LCD_Write_Command(data_byte);
    // Delay_10us(10);



} /* LCD_init() */

/* Clear LCD Display and Reset Display Position to 0 */
void LCD_Clear(void)
{
    /*----------------
    |  DISPLAY CLEAR  |
    ----------------*/

    LCD_Write_Command(0x01);
    Delay_1ms(10);

    /*---------------------------------------------------
    |  RESET CURSOR POSITION/DISPLAY CHAR POSITION TO 0  |
    ---------------------------------------------------*/
    LCD_Write_Command( LCD_CMD_SET_DISP_POS | 0x00 );
    Delay_1ms(10);
}

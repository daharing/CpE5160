/*
* Author:       Dustin Haring
* Copyright:    Copyright (C) 2019 Dustin Haring - All Rights Reserved.
* Date Created: Sept. 24, 2019
* Desc:         Header file for various helper/utility functions
*/


#ifndef HELPER_FUNCTIONS_H
#define HELPER_FUNCTIONS_H

/*-----------
|  INCLUDES  |
-----------*/

#include "Main.h"


/*----------
|  DEFINES  |
----------*/

#define LOOPS_PER_10US  (8)          /* Loop iterations per microsecond */
#define LOOPS_PER_MS    (1244)       /* Loop iterations per millisecond */


/*------------------------
|  FUNCTION DECLARATIONS  |
------------------------*/

/* Give a byte, this function returns the byte with all 
    bits reversed (aka bit 0 -> bit 7...) */
uint8_t reverseBits_8(uint8_t byte_to_reverse);

/* Fairly inaccurate delay but good enough for now. >1.0002ms per iteration */
void Delay_1ms(uint16_t num_ms);

/* Fairly inaccurate delay but good enough for now. >9.9us per iteration */
void Delay_10us(uint16_t num_10us);

#endif /* HELPER_FUNCTIONS_H */

/*
* Author:       Dustin Haring
* Copyright:    Copyright (C) 2019 Dustin Haring - All Rights Reserved.
* Date Created: Sept. 24, 2019
* Desc:     Utility/helper functions for various tasks
*/

/*-------------------
|  PRIVATE INCLUDES  |
-------------------*/

#include "helper_functions.h"


/*-----------------------
|  FUNCTION DEFINITIONS  |
-----------------------*/

uint8_t reverseBits_8(uint8_t byte_to_reverse) 
{ 
    /*------------
    |  VARIABLES  |
    ------------*/

    uint8_t count; 
    uint8_t reversed_byte;


    /*------------------
    |  INITIALIZATIONS  |
    ------------------*/
      
    count = sizeof(byte_to_reverse) * 8 - 1; 
    reversed_byte = byte_to_reverse; 
    byte_to_reverse >>= 1;  
    

    /*---------------
    |  REVERSE BITS  |
    ---------------*/

    while(byte_to_reverse) 
    { 
       reversed_byte <<= 1;        
       reversed_byte |= byte_to_reverse & 1; 
       byte_to_reverse >>= 1; 
       count--; 
    } /* while() */

    reversed_byte <<= count; 
    
    return reversed_byte; 
} /* reverseBits_8 */

void Delay_1ms(uint16_t num_ms)
{
    /*------------
    |  VARIABLES  |
    ------------*/
    uint16_t i;
    uint16_t j;

    /*---------------
    |  DELAY NUM_MS  |
    ---------------*/

    for (i = 0; i < num_ms; i++)
    {
        for (j = 0; j < LOOPS_PER_MS; j++); /* This totals 1.0002 ms */
    } /* for () */
    
} /* Delay_1ms */

void Delay_10us(uint16_t num_10us)
{
    /*------------
    |  VARIABLES  |
    ------------*/

    uint16_t i;
    uint16_t j;
    
    
    /*------------------------------------
    |  DELAY NUM_10US MULTIPLES OF 10 US  |
    ------------------------------------*/
    
    for (i = 0; i < num_10us; i++)
    {
        for (j = 0; j < LOOPS_PER_10US; j++); /* This totals 9.9 us */
    } /* for () */
    
} /* Delay_10us */

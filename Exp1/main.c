/*
* Author:       Dustin Haring
* Copyright:    Copyright (C) 2019 Dustin Haring - All Rights Reserved.
* Date Created: Sept. 24, 2019
* Desc: 
*/

#include "Main.h"

/*-------------------
|  LIBRARY INCLUDES  |
-------------------*/
#include "port.h"
#include <stdio.h>
#include <string.h> /* for strlen */
#include "UART.h"

/*----------------
|  USER INCLUDES  |
----------------*/
#include "memory_test.h"
#include "print_bytes.h"
#include "LCD.h"

/*-------------------
|  DEFINE CONSTANTS  |
-------------------*/
#define UART_BAUD_RATE (9600UL)		/* The UART baud rate used */

/*-----------------
|  DEFINE STATICS  |
-----------------*/

static uint8_t code LCD_str[] = "LCD test DAHARING";


/*-----------------------
|  FUNCTION DEFINITIONS  |
-----------------------*/

/* All things necessary to initialize and setup 
    the MCU and program(s) */
void system_init( void )
{
    /*-------------------------------
    |  MISCELLANEOUS CONFIGURATIONS  |
    -------------------------------*/

    /* Disable all interrupts */
    ES = 0;

    /* Disable UART interrupts */
    EA = 0;

    /* Double the clock rate (x2 mode) */
    CKCON0 = 0x01; // 0b00000001;


    /*-------------------------
    |  DRIVER INITIALIZATIONS  |
    -------------------------*/

    UART_init( (uint32_t)UART_BAUD_RATE );
    LCD_init();

} /* system_init() */


/* Program Execution */
void system_execute( void )
{
    /*------------
    |  VARIABLES  |
    ------------*/

    uint8_t rec_char;
    uint8_t *mem_ptr;


    /*------------------
    |  INITIALIZATIONS  |
    ------------------*/

    rec_char = 0;
    mem_ptr = NULL;


    /*-----------------------
    |   MAIN PROGRAM LOOP    |
    -----------------------*/

    while(1)
    {
         /*----------------------
        |  WRITE STRING TO LCD  |
        ----------------------*/
        
        LCD_Clear();
        LCD_Print_String(LCD_str, strlen(LCD_str));
        

        /*--------------------------------
        |  TEST PRINTF, PUTCHAR, GETCHAR  |
        |   UART_TX, UART_RX FUNCTIONS    |
        --------------------------------*/

        /* Test printf and putchar */
        printf("Test printf");
        putchar('!');
        putchar('\n');

        /* Test UART_TX() */
        UART_TX('1');
        UART_TX('2');
        UART_TX('3');
        UART_TX('4');
        UART_TX('\n');
        UART_TX('\r');
        
        /* Test getchar and UART_RX() */
            /* NOTE getchar will echo the received character to UART.*/
        getchar();
        
        rec_char = UART_RX();
        putchar(rec_char);
        
        rec_char = UART_RX();
        UART_TX(rec_char);

        UART_TX('\n');
        UART_TX('\n');
        UART_TX('\r');


        /*----------------------------------------------------
        |  INITIALIZE CODE MEMORY BLOCK AND PRINT THE STRING  |
        ----------------------------------------------------*/

        mem_ptr = code_memory_init();
        print_memory(mem_ptr, strlen(mem_ptr));


        /*----------------------------------------------
        |  INITIALIZE THE XDATA MEMORY BLOCK AND PRINT  |
        ----------------------------------------------*/

        mem_ptr = xdata_memory_init();
        print_memory(mem_ptr, strlen(mem_ptr));

    } /* while(1) */
} /* system_execute() */


/* Provides a safe shutdown */
void system_shutdown( void )
{
    /* At least for now we should never end up here */
    while(1);
} /* system_shutdown() */


int main( void )
{
    /*--------------------
    |  INITIALIZE SYSTEM  |
    --------------------*/
    system_init();

    /*----------------------
    |  EXECUTE SYSTEM CODE  |
    ----------------------*/
    system_execute();

    /*-------------------------
    |  SHUTDOWN SYSTEM SAFELY  |
    -------------------------*/
    system_shutdown();

    return 0;
}
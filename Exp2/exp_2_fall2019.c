/*-------------------------------------
|           PRIVATE INCLUDES           |
--------------------------------------*/

#include "exp_2_fall2019.h"
#include <stdio.h>
#include "PORT.H"
#include "UART.h"
#include "print_bytes.h"
#include "Timer0_hardware_delay_1ms.h"
#include "LCD_routines.h"
#include "Outputs.h"
#include "Long_Serial_In.h"
#include "SPI_Driver.h"
#include "SDCard.h"

/*-------------------------------------
|           PRIVATE STATICS            |
--------------------------------------*/

static xdata uint8_t SD_Buff[SD_BLOCK_SZ] = {0};


/*-------------------------------------
|         FUNCTION DEFINITIONS         |
--------------------------------------*/

int8_t exp2_init()
{
    /*-------------------------------------
    |    CONFIGURE XRAM ACCESS FOR 1KiB    |
    --------------------------------------*/

    AUXR = 0x0C; /* 0b00001100 -> make all 1024 bytes of XRAM available, ALE always on */


    /*-------------------------------------
    |           SET SYSTEM CLOCK           |
    --------------------------------------*/

    if (OSC_PER_INST == 6)
    {
        CKCON0 = 0x01; // set X2 clock mode
    }
    else if (OSC_PER_INST == 12)
    {
        CKCON0 = 0x00; // set standard clock mode
    }


    /*-------------------------------------
    |        DRIVER INITIALIZATIONS        |
    --------------------------------------*/

    uart_init(UART_BAUD_RATE);
    LCD_Init();

    if ( SD_RETURN_SUCCESS != SPI_Master_Init(SPI_INIT_CLK_RATE) )
    {
        printf("Failed to init SPI!\n\r");
        return RETURN_FAILED;
    }

    if ( SD_RETURN_SUCCESS != SD_Card_init() )
    {
        printf("Failed to init SD Card!\n\r");
        return RETURN_FAILED;
    }

    if ( SD_RETURN_SUCCESS != SPI_Master_Init(SPI_RUN_CLK_RATE) )
    {
        printf("Failed to increase SPI CLock Rate!\n\r");
        return RETURN_FAILED;
    }

    return RETURN_SUCCESS;
} /* exp2_init() */



int8_t exp2_execute()
{
    /*-------------------------------------
    |              VARIABLES               |
    --------------------------------------*/

    uint32_t block, LBA;
    uint8_t temp;


    /*-------------------------------------
    |            INITIAL PRINT             |
    --------------------------------------*/

    LCD_Print(line1, 13, (uint8_t *)"DAHARING V1.0");


    /*-------------------------------------
    |           GET SD CARD TYPE           |
    --------------------------------------*/

    temp = SD_Card_get_type();

    if ( temp == SD_TYPE_STANDARD )
    {
        LCD_Print(line2, 0, "SD Std. Capacity");
    }
    else if ( temp == SD_TYPE_HIGH_CAPACITY )
    {
        LCD_Print(line2, 0, "SD High Capacity");
    }

    /*-------------------------------------
    |            INFINITE LOOP             |
    --------------------------------------*/

    while (1)
    {
        printf("Input a block number to read: ");
        block = long_serial_input();

        printf("Reading SD Card block number %lu\n\r", block);
        LCD_Print(line2, 0, "Reading SD Card ");

        LBA = block << SD_Card_get_type();

        LED_On(Yellow_LED);
        Port_Clear_bits(PORT_1, nCS0_bit);
        
        temp = SD_Card_send_cmd(SD_CMD17, LBA);

        if (temp != SD_RETURN_SUCCESS)
        {
            printf("ERROR: SD Card command FAILED\n\r");
            LCD_Print(line1, 0, "Exiting Program:");
            LCD_Print(line2, 0, "SD CMD ERROR!   ");
            return RETURN_FAILED;
        }

        temp = SD_Card_read_block(SD_BLOCK_SZ, SD_Buff);

        Port_Set_bits(PORT_1, nCS0_bit);

        if (temp != SD_RETURN_SUCCESS)
        {
            LCD_Print(line1, 0, "Exiting Program:");
            LCD_Print(line2, 0, "SD Read ERROR!  ");
            printf("ERROR: SD Card block read FAILED\n\r");
            return RETURN_FAILED;
        }

        LED_Off(Yellow_LED);

        print_memory(SD_Buff, SD_BLOCK_SZ);

        LCD_Print(line2, 0, "Read SD Card!   ");
    } /* infinite loop */

    return RETURN_SUCCESS;
} /* exp2_execute() */



void exp2_failure()
{
SYSTEM_FAILED:


    /*-------------------------------------
    |           SET ALL LEDS ON            |
    --------------------------------------*/
    LED_On(Red_LED);
    // LED_On(Yellow_LED);
    // LED_On(Amber_LED);
    // LED_On(Green_LED);


    /*-------------------------------------
    |            INFINITE LOOP             |
    --------------------------------------*/

    for ( ; ; )
    {
        
    }

} /* exp2_failure() */

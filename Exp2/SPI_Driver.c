/*-------------------------------------
|           PRIVATE INCLUDES           |
--------------------------------------*/

#include "SPI_Driver.h"
#include "PORT.H"


/*-------------------------------------
|         FUNCTION DEFINITIONS         |
--------------------------------------*/

uint8_t SPI_Master_Init(uint32_t clock_rate)
{
    uint8_t divider;

    divider = (OSC_FREQ * 6 / (OSC_PER_INST * clock_rate));
    if (divider <= 2)
    {
        SPCON = 0x70 | (CPOL << 3) | (CPHA << 2);
    }
    else if (divider <= 4)
    {
        SPCON = 0x71 | (CPOL << 3) | (CPHA << 2);
    }
    else if (divider <= 8)
    {
        SPCON = 0x72 | (CPOL << 3) | (CPHA << 2);
    }
    else if (divider <= 16)
    {
        SPCON = 0x73 | (CPOL << 3) | (CPHA << 2);
    }
    else if (divider <= 32)
    {
        SPCON = 0xF0 | (CPOL << 3) | (CPHA << 2);
    }
    else if (divider <= 64)
    {
        SPCON = 0xF1 | (CPOL << 3) | (CPHA << 2);
    }
    else if (divider <= 128)
    {
        SPCON = 0xF2 | (CPOL << 3) | (CPHA << 2);
    }
    else
    {
        return SPI_RETURN_ERROR;
    }

    return SPI_RETURN_SUCCESS;
}

uint8_t SPI_Transfer(uint8_t tx_value, uint8_t *rx_value)
{
    /*-------------------------------------
    |              VARIABLES               |
    --------------------------------------*/

    uint8_t temp, timeout;
    
    /*-------------------------------------
    |           INITIALIZATIONS            |
    --------------------------------------*/

    timeout = 0;


    /*-------------------------------------
    |            SEND TX_VALUE             |
    --------------------------------------*/

    SPDAT = tx_value;

    do
    {
        temp = SPSTA;
        timeout++;
    } while (((temp & 0xF0) == 0) && (timeout != 0));


    /*-------------------------------------
    |            RECEIVE VALUE             |
    --------------------------------------*/

    if (timeout != 0)
    {
        if ((temp & 0x70) == 0)
        {
            *rx_value = SPDAT;
            timeout = SPI_RETURN_SUCCESS;
        }
        else
        {
            *rx_value = 0xff;
            timeout = SPI_RETURN_ERROR;
        }
    }
    else
    {
        *rx_value = 0xff;
        timeout = SPI_RETURN_ERROR;
    }
    return timeout;
}

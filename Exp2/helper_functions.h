/*
* Author:       Dustin Haring
* Copyright:    Copyright (C) 2019 Dustin Haring - All Rights Reserved.
* Date Created: Sept. 24, 2019
* Desc:         Header file for various helper/utility functions
*/


#ifndef HELPER_FUNCTIONS_H
#define HELPER_FUNCTIONS_H

/*-----------
|  INCLUDES  |
-----------*/

#include "Main.h"


/*----------
|  DEFINES  |
----------*/


/*------------------------
|  FUNCTION DECLARATIONS  |
------------------------*/

/* Given a byte, this function returns the byte with all 
    bits reversed (aka bit 0 -> bit 7, bit 1 -> bit 6...) */
uint8_t reverseBits_8(uint8_t byte_to_reverse);


#endif /* HELPER_FUNCTIONS_H */

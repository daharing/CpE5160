/*-------------------------------------
|           PRIVATE INCLUDES           |
--------------------------------------*/
#include "exp_5_fall2019.h"
#include <stdio.h>
#include "PORT.H"
// #include "Outputs.h"

/*-------------------------------------
|           PRIVATE DEFINES            |
--------------------------------------*/
#define INTERVAL_MS             (10) /* How many ms between program executions. Also, twice this value is the debouch time */
#define PRELOAD                 (65536-((OSC_FREQ * INTERVAL_MS)/(OSC_PER_INST * 1000UL)))
#define BASE_LED_WALK_TIME_MS   (6*INTERVAL_MS) /* Base value of ms between LED flashes for left to right and right to left led illuminations */
#define false   0
#define on      0
#define true    1
#define off     1

/*-------------------------------------
|    PRIVATE FUNCTION DECLARATIONS     |
--------------------------------------*/
void app_tick_execute();

/*-------------------------------------
|            PRIVATE ENUMS             |
--------------------------------------*/
typedef enum
{
    idle = 0,
    delay_adjustment,

    // LEDs_Finished,  /* Indicates LED sequence complete */
    LEDs_RTL,       /* Light leds right to left */
    LEDs_RTL__1,    /* Light leds right to left */
    LEDs_RTL__2,    /* Light leds right to left */
    LEDs_RTL__3,    /* Light leds right to left */

    LEDs_LTR,       /* Light leds left to right */
    LEDs_LTR__1,    /* Light leds right to left */
    LEDs_LTR__2,    /* Light leds right to left */
    LEDs_LTR__3,    /* Light leds right to left */
} app_state_t;

typedef enum
{
    not_pressed = 0,
    debouncing,
    pressed,
    // hold,
    released
} switch_state_t;

/*-------------------------------------
|           PRIVATE STATICS            |
--------------------------------------*/
static app_state_t app_state = idle;                    /* Keeps track of the application state */
static switch_state_t sw_states[4] = {not_pressed};     /* Keeps track of the switch states. Index 0 == sw1 ... index 3 == sw4 */
static uint8_t delay_multiplier = 1;                    /* Multiples of BASE_LED_WALK_TIME_MS for LED lighting sequency delays */
static uint16_t time_counter_ms = 0;                    /* Keeps track of time for state determinations */

/*-------------------------------------
|         FUNCTION DEFINITIONS         |
--------------------------------------*/

int8_t exp5_init()
{
    /*-------------------------------------
    |        DELAY FOR RANDOM TIME         |
    --------------------------------------*/
     uint16_t i = 0;
     while( i < 65000 ) { i++; }

    /*-------------------------------------
    |    CONFIGURE XRAM ACCESS FOR 1KiB    |
    --------------------------------------*/

    // AUXR = 0x0C; /* 0b00001100 -> make all 1024 bytes of XRAM available, ALE always on */


    /*-------------------------------------
    |           SET SYSTEM CLOCK           |
    --------------------------------------*/

    if (OSC_PER_INST == 6)
    {
        CKCON0 = 0x01; /* Set X2 clock mode */
    }
    else if (OSC_PER_INST == 12)
    {
        CKCON0 = 0x00; /* Set standard clock mode */
    }


    /*-------------------------------------
    |        DRIVER INITIALIZATIONS        |
    --------------------------------------*/

    // uart_init(UART_BAUD_RATE);
    // LCD_Init();

    // if ( SD_RETURN_SUCCESS != SPI_Master_Init(SPI_INIT_CLK_RATE) )
    // {
    //     printf("Failed to init SPI!\n\r");
    //     return RETURN_FAILED;
    // }

    // if ( SD_RETURN_SUCCESS != SD_Card_init() )
    // {
    //     printf("Failed to init SD Card!\n\r");
    //     return RETURN_FAILED;
    // }

    // if ( SD_RETURN_SUCCESS != SPI_Master_Init(SPI_RUN_CLK_RATE) )
    // {
    //     printf("Failed to increase SPI CLock Rate!\n\r");
    //     return RETURN_FAILED;
    // }

    // if ( ERR_NONE != STA013_Init() )
    // {
    //     printf("Failed to init STA013!\n\r");
    //     return RETURN_FAILED;
    // }

    /*-------------------------------------
    |     APPLICATION INITIALIZATIONS      |
    --------------------------------------*/
    T2CON  = 0;
    RCAP2H = PRELOAD/256;
    RCAP2L = PRELOAD%256;
    TF2 = 0;
    ET2 = 1;
    EA = 1;
    TR2 = 1;

    return RETURN_SUCCESS;
} /* exp5_init() */

int8_t exp5_execute()
{
    /*-------------------------------------
    |              VARIABLES               |
    --------------------------------------*/

    /*-------------------------------------
    |      SETUP SWITCH PINS TO INPUT      |
    --------------------------------------*/
    SW1 = 1;
    SW2 = 1;
    SW3 = 1;
    SW4 = 1;

    /*-------------------------------------
    |           INITIALIZE LEDS            |
    --------------------------------------*/
    REDLED = off;
    GREENLED = off;
    YELLOWLED = off;
    AMBERLED = off;

    /*-------------------------------------
    |            INFINITE LOOP             |
    --------------------------------------*/
    while(1)
    {
        /* Put processor to sleep */
        // PCON |= 0x01;
    }


    return RETURN_SUCCESS;
} /* exp5_execute() */

void exp5_failure()
{
SYSTEM_FAILED:


    /*-------------------------------------
    |              SET LEDS                |
    --------------------------------------*/
    REDLED = on;
    YELLOWLED = on;
    AMBERLED = on;
    GREENLED = on;


    /*-------------------------------------
    |            INFINITE LOOP             |
    --------------------------------------*/

    for ( ; ; )
    {
        
    }

} /* exp5_failure() */

void app_tick_execute()
{
    /*-------------------------------------
    |              VARIABLES               |
    --------------------------------------*/
    switch_state_t *sw_state;
    uint8_t isSWPressed;
    uint8_t did_sw_state_change[4] = {false,false,false,false}; /* array element for each switch representing if it had a state change */
    uint8_t i;

    /*-------------------------------------
    |           INC TIME COUNTER           |
    --------------------------------------*/
    time_counter_ms += INTERVAL_MS;

    /*-------------------------------------
    |         UPDATE BUTTON STATES         |
    --------------------------------------*/
    for (i = 0; i < 4; i++)
    {
        /* Get stored switch state */
        sw_state = sw_states + i;

        /* get current gpio value for switch */
        if(i == 0)
        {
            isSWPressed = SW1;
        }
        else if(i == 1)
        {
            isSWPressed = SW2;
        }
        else if(i == 2)
        {
            isSWPressed = SW3;
        }
        else if(i == 3)
        {
            isSWPressed = SW4;
        }

        switch (*sw_state)
        {
        case not_pressed:
            if(isSWPressed == 0) /* sw is pressed */
            {
                *sw_state = debouncing;
                did_sw_state_change[i] = true;
            }
            break;
        case debouncing:
            if(isSWPressed == 0) /* sw is pressed */
            {
                *sw_state = pressed;
                did_sw_state_change[i] = true;
            }
            else
            {
                *sw_state = not_pressed;
                did_sw_state_change[i] = true;
            }
            break;
        case pressed:
            if(isSWPressed == 1) /* sw is not pressed */
            {
                *sw_state = released;
                did_sw_state_change[i] = true;
            }
            break;
        case released:
            if(isSWPressed == 0) /* sw is pressed */
            {
                *sw_state = debouncing;
                did_sw_state_change[i] = true;
            }
            else /* sw is not pressed */
            {
                *sw_state = not_pressed;
                did_sw_state_change[i] = true;
            }
            break;

        default:
            exp5_failure();
            break;
        }
    }

    /*-------------------------------------
    |  PROCESS CURRENT APPLICATION STATE   |
    |   HANDLE ANY BUTTON/SWITCH EVENTS    |
    --------------------------------------*/
    switch (app_state)
    {
    case idle:
        if(did_sw_state_change[0] == true) /* sw1 */
        {
            if(sw_states[0] == pressed)
            {
                time_counter_ms = 0;
                app_state = LEDs_LTR;
            }
        }
        else if(did_sw_state_change[1] == true) /* sw2 */
        {
            if(sw_states[1] == pressed)
            {
                app_state = delay_adjustment;
            }
        }
        else if(did_sw_state_change[2] == true) /* sw3 */
        {
            if(sw_states[2] == pressed)
            {
                app_state = delay_adjustment;
            }
        }
        else if(did_sw_state_change[3] == true) /* sw4 */
        {
            if(sw_states[3] == pressed)
            {
                time_counter_ms = 0;
                app_state = LEDs_RTL;
            }
        }
        break;

    case LEDs_LTR:
        /* Update LEDs if needed */
        if(time_counter_ms >= BASE_LED_WALK_TIME_MS*delay_multiplier)
        {
            /* Set LED and change states */
            time_counter_ms = 0;
            REDLED = on;
            app_state = LEDs_LTR__1;
        }

        /* Handle sw event if needed */
        if(did_sw_state_change[0] == true) /* sw4 */
        {
            if(sw_states[0] == pressed)
            {
                REDLED = off;
                GREENLED = off;
                AMBERLED = off;
                YELLOWLED = off;
                app_state = idle;
            }
        }
        break;
    case LEDs_LTR__1:
        if(time_counter_ms >= BASE_LED_WALK_TIME_MS*delay_multiplier)
        {
            /* Set LED and change states */
            time_counter_ms = 0;
            GREENLED = on;
            app_state = LEDs_LTR__2;
        }

        /* Handle sw event if needed */
        if(did_sw_state_change[0] == true) /* sw4 */
        {
            if(sw_states[0] == pressed)
            {
                REDLED = off;
                GREENLED = off;
                AMBERLED = off;
                YELLOWLED = off;
                app_state = idle;
            }
        }

        break;

    case LEDs_LTR__2:
        if(time_counter_ms >= BASE_LED_WALK_TIME_MS*delay_multiplier)
        {
            /* Set LED and change states */
            time_counter_ms = 0;
            YELLOWLED = on;
            app_state = LEDs_LTR__3;
        }

        /* Handle sw event if needed */
        if(did_sw_state_change[0] == true) /* sw4 */
        {
            if(sw_states[0] == pressed)
            {
                REDLED = off;
                GREENLED = off;
                AMBERLED = off;
                YELLOWLED = off;
                app_state = idle;
            }
        }
    case LEDs_LTR__3:
        if(time_counter_ms >= 2*BASE_LED_WALK_TIME_MS*delay_multiplier)
        {
            /* Set LEDs and change states */
            time_counter_ms = 0;
            REDLED = off;
            GREENLED = off;
            AMBERLED = off;
            YELLOWLED = off;
            app_state = LEDs_LTR;
        }
        else if(time_counter_ms >= BASE_LED_WALK_TIME_MS*delay_multiplier)
        {
            /* Set LED */
            AMBERLED = on;
        }

        /* Handle sw event if needed */
        if(did_sw_state_change[0] == true) /* sw4 */
        {
            if(sw_states[0] == pressed)
            {
                REDLED = off;
                GREENLED = off;
                AMBERLED = off;
                YELLOWLED = off;
                app_state = idle;
            }
        }

        break;


    case LEDs_RTL:
        /* Update LEDs if needed */
        if(time_counter_ms >= BASE_LED_WALK_TIME_MS*delay_multiplier)
        {
            /* Set LED and change states */
            time_counter_ms = 0;
            AMBERLED = on;
            app_state = LEDs_RTL__1;
        }

        /* Handle sw event if needed */
        if(did_sw_state_change[0] == true) /* sw1 */
        {
            if(sw_states[3] == pressed)
            {
                REDLED = off;
                GREENLED = off;
                YELLOWLED = off;
                AMBERLED = off;
                app_state = idle;
            }
        }
        break;
    case LEDs_RTL__1:
        if(time_counter_ms >= BASE_LED_WALK_TIME_MS*delay_multiplier)
        {
            /* Set LED and change states */
            time_counter_ms = 0;
            YELLOWLED = on;
            app_state = LEDs_RTL__2;
        }

        /* Handle sw event if needed */
        if(did_sw_state_change[3] == true) /* sw1 */
        {
            if(sw_states[3] == pressed)
            {
                REDLED = off;
                GREENLED = off;
                AMBERLED = off;
                YELLOWLED = off;
                app_state = idle;
            }
        }

        break;

    case LEDs_RTL__2:
        if(time_counter_ms >= BASE_LED_WALK_TIME_MS*delay_multiplier)
        {
            /* Set LED and change states */
            time_counter_ms = 0;
            GREENLED = on;
            app_state = LEDs_RTL__3;
        }

        /* Handle sw event if needed */
        if(did_sw_state_change[3] == true) /* sw1 */
        {
            if(sw_states[3] == pressed)
            {
                REDLED = off;
                GREENLED = off;
                AMBERLED = off;
                YELLOWLED = off;
                app_state = idle;
            }
        }
    case LEDs_RTL__3:
        if(time_counter_ms >= 2*BASE_LED_WALK_TIME_MS*delay_multiplier)
        {
            /* Set LEDs and change states */
            time_counter_ms = 0;
            REDLED = off;
            GREENLED = off;
            AMBERLED = off;
            YELLOWLED = off;
            app_state = LEDs_RTL;
        }
        else if(time_counter_ms >= BASE_LED_WALK_TIME_MS*delay_multiplier)
        {
            /* Set LED */
            REDLED = on;
        }

        /* Handle sw event if needed */
        if(did_sw_state_change[3] == true) /* sw1 */
        {
            if(sw_states[3] == pressed)
            {
                REDLED = off;
                GREENLED = off;
                AMBERLED = off;
                YELLOWLED = off;
                app_state = idle;
            }
        }

        break;

    case delay_adjustment:
        /* Inc delay multiplier value */
        if(did_sw_state_change[1] == true && sw_states[1] == pressed) /* sw2 */
        {
            delay_multiplier += 1;
            if(delay_multiplier >= 15)
            {
                delay_multiplier = 15;
            }
        }

        /* Dec delay multipler value */
        if(did_sw_state_change[2] == true && sw_states[2] == pressed) /* sw3 */
        {
            delay_multiplier -= 1;
            if(delay_multiplier == 0)
            {
                delay_multiplier = 1;
            }
        }

        /* Set LEDs to indicate, in binary format with RED == MSb, the value of delay_multiplier */
        if((delay_multiplier & 0x01) == 0x01)
        {
            AMBERLED = on;
        }
        else
        {
            AMBERLED = off;
        }
        if((delay_multiplier & 0x02) == 0x02)
        {
            YELLOWLED = on;
        }
        else
        {
            YELLOWLED = off;
        }
        if((delay_multiplier & 0x04) == 0x04)
        {
            GREENLED = on;
        }
        else
        {
            GREENLED = off;
        }
        if((delay_multiplier & 0x08) == 0x08)
        {
            REDLED = on;
        }
        else
        {
            REDLED = off;
        }

        /* Check for state exit condition */
        if(did_sw_state_change[0] == true || did_sw_state_change[3] == true) /* sw1 or sw4 */
        {
            if(sw_states[0] == pressed || sw_states[3] == pressed)
            {
                REDLED = off;
                GREENLED = off;
                AMBERLED = off;
                YELLOWLED = off;
                app_state = idle;
            }
        }
        break;

    default:
        exp5_failure();
        break;
    }

} /* app_tick_execute()


/*-----------------------------------------
|  APPLICATION INTERRUPT SERVICE ROUTINES  |
------------------------------------------*/

void exp5_ISR(void) interrupt 5 using 1
{
    TF2 = 0; /* Reset interrupt */
    app_tick_execute();
} /* exp5_ISR() */

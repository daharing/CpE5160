#ifndef EXP_2_FALL2019_H
#define EXP_2_FALL2019_H

/*-------------------------------------
|               INCLUDES               |
--------------------------------------*/

#include "Main.h"


/*-------------------------------------
|            PUBLIC DEFINES            |
--------------------------------------*/

#define RETURN_SUCCESS          (0)
#define RETURN_FAILED           (-1)


/*-------------------------------------
|           PUBLIC FUNCTIONS           |
--------------------------------------*/

/**
 * This function performs any and all initializations
 * needed for the system and user code.
 * 
 * @Returns SPI_RETURN_SUCCESS or RETURN_FAILED
 */
int8_t exp5_init();

/**
 * Execute user code (Experiment 2 for CpE 5160 2019F with
 * Younger). Function should never return.
 * 
 * @Returns SPI_RETURN_SUCCESS or RETURN_FAILED
 */
int8_t exp5_execute();

/**
 * This function will put the system in a safe and defined 
 * state if exp2_execute() returns. In our implementation,
 * it will illuminate all LEDs to indicate failure.
 */
void exp5_failure();


#endif /* EXP_2_FALL2019_H */
/*
* Author:       Dustin Haring
* Copyright:    Copyright (C) 2019 Dustin Haring - All Rights Reserved.
* Date Created: Sept. 24, 2019
* Desc:     Utility/helper functions for various tasks
*/

/*-------------------
|  PRIVATE INCLUDES  |
-------------------*/

#include "helper_functions.h"


/*-----------------------
|  FUNCTION DEFINITIONS  |
-----------------------*/

uint8_t reverseBits_8(uint8_t byte_to_reverse) 
{ 
    /*------------
    |  VARIABLES  |
    ------------*/

    uint8_t count; 
    uint8_t reversed_byte;


    /*------------------
    |  INITIALIZATIONS  |
    ------------------*/
      
    count = sizeof(byte_to_reverse) * 8 - 1; 
    reversed_byte = byte_to_reverse; 
    byte_to_reverse >>= 1;  
    

    /*---------------
    |  REVERSE BITS  |
    ---------------*/

    while(byte_to_reverse) 
    { 
       reversed_byte <<= 1;        
       reversed_byte |= byte_to_reverse & 1; 
       byte_to_reverse >>= 1; 
       count--; 
    } /* while() */

    reversed_byte <<= count; 
    
    return reversed_byte; 
} /* reverseBits_8() */

#ifndef SD_CARD_H
#define SD_CARD_H


/*-------------------------------------
|           PUBLIC INCLUDES            |
--------------------------------------*/

#include "Main.H"


/*-------------------------------------
|            PUBLIC DEFINES            |
--------------------------------------*/

#define SD_BLOCK_SZ                 (512)

/* SD Card Commands */
#define SD_CMD0                     (0)
#define SD_CMD8                     (8)
#define SD_CMD16                    (16)
#define SD_CMD17                    (17)
#define SD_CMD55                    (55)
#define SD_CMD58                    (58)
#define SD_ACMD41                   (41)

/* SD Card types */
#define SD_TYPE_HIGH_CAPACITY       (0X00)
#define SD_TYPE_VERSION_2           (0X02)
#define SD_TYPE_STANDARD            (0X09)
#define SD_TYPE_UNK                 (0xFF)

/* error return values */
#define SD_RETURN_SUCCESS           (0)
#define SD_RETURN_ERR_TIMEOUT       (0x81)
#define SD_RETURN_ERR_ILLEGAL_CMD   (0x82)
#define SD_RETURN_ERR_RESPONSE      (0x83)
#define SD_RETURN_ERR_DATA          (0x84)
#define SD_RETURN_ERR_VOLTAGE       (0x85)
#define SD_RETURN_INACTIVE          (0x86)
#define SD_RETURN_ERR_SPI           (0x87)

/* SD Card bits */
#define nCS0_bit     (1 << 4)


/*-------------------------------------
|     PUBLIC FUNCTION DECLARATIONS     |
--------------------------------------*/

/**
 * 
 */
uint8_t SD_Card_init();

/**
 * 
 */
uint8_t SD_Card_send_cmd(uint8_t command, uint32_t arg);

/**
 * 
 */
uint8_t SD_Card_recv_response(uint8_t num, uint8_t * recv_arry);

/**
 * 
 */
uint8_t SD_Card_read_block(uint16_t num, uint8_t * array_out_p);

/**
 * 
 */
uint8_t SD_Card_get_type();

/**
 * 
 */
void SD_Card_print_error(uint8_t error);


#endif /* SD_CARD_H */

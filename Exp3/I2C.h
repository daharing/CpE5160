#ifndef I2C_H
#define I2C_H

/*-------------------------------------
|           PUBLIC INCLUDES            |
--------------------------------------*/
#include "Main.H"

/*-------------------------------------
|            PUBLIC DEFINES            |
--------------------------------------*/
#define ERR_NONE        (0)
#define ERR_UNK         (1)
#define ERR_TIMEOUT     (2)
#define ERR_BUS_BUSY    (0x81)
#define ERR_NACK        (0x82)


/*-------------------------------------
|     PUBLIC FUNCTION DECLARATIONS     |
--------------------------------------*/

/**
 * Receives data over I2C
 * 
 * @Returns Error flag
 */
// uint8_t I2C_Read(uint8_t device_addr,uint32_t internal_addr, uint8_t internal_addr_sz, uint8_t nBytes,uint8_t *recv_arry);
uint8_t I2C_Read(uint8_t device_addr,uint32_t int_addr, uint8_t int_addr_sz, uint8_t num_bytes,uint8_t * rec_array);

/**
 * Transmits data over I2C
 * 
 * @Returns Error flag
 */
uint8_t I2C_Write(uint8_t device_addr,uint32_t internal_addr, uint8_t internal_addr_sz, uint8_t nBytes,uint8_t *send_arry);

#endif /* I2C_H */
